/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.sheridancollege.softwarefundamentals.pickacard;

/**
 * A class that fills a magic hand of 7 cards with random Card Objects
 * and then asks the user to pick a card and searches the array of cards
 * for the match to the user's card. To be used as starting code in ICE 1
 * @author dancye
 */
import java.util.Scanner;
public class CardTrick {
    
    public static void main(String[] args)
    {
        Card[] magicHand = new Card[7];
        Scanner kb = new Scanner (System.in);
        int number;
        String suite;
        
        for (int i=0; i<magicHand.length; i++)
        {
        
            magicHand[i]=new Card(Card.SUITS[(int)(Math.random()*4)],(int)((Math.random()*13)+1));
            //c.setValue(insert call to random number generator here)
            //c.setSuit(Card.SUITS[insert call to random number between 0-3 here])
        }
        System.out.println("Guess a number (1-13)");
        number=kb.nextInt();
        System.out.println("Guess the Suit, 'Heart','Spade','Diamond', 'Club' ");
        suite=kb.next();
        
        for (int i=0;i<7;i++){
            if(suite.equalsIgnoreCase(magicHand[i].getSuit())){
                if(number==magicHand[1].getValue()){
                    System.out.print("YOU GUESS RIGHT");
                }
            }
            if(i>=6){
                System.out.print("The Card were: ");
                 for (int x=0; x<magicHand.length; x++)
        {
           System.out.println(magicHand[x].getValue()+magicHand[x].getSuit() );
        }
            }
        }
        
        
        
        //insert code to ask the user for Card value and suit, create their card
        // and search magicHand here
        //Then report the result here
    }
    
}
